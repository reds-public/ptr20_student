#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>

#include <cobalt/stdio.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/mutex.h>

#include "io_utils.h"
#include "board.h"
#include "busycpu.h"

#define SIGNAL_BUF_SIZE     (1 << 13) // In number of samples
#define NB_WAVES            3

static void *ioctrls;
static int rtsnd_fd;
static data_t *wave[NB_WAVES];      // different types of waves over a period
static double wave_idx[NB_WAVES];   // Indicates where we are in the wave
static int current_wave = SINUSOID;
static double current_freq = FREQ0;

/* Simulated read buffer
 *
 *  0  1  2  3  4  5  6  7
 * -------------------------
 * |  |  |x1|x2|x3|x4|  |  |
 * -------------------------
 *
 * tail: 2
 * head: 6
 * capacity: 8
 * size: 4
 */
struct read_buffer {
    data_t *buffer;
    size_t capacity;
    size_t size;
    size_t head;
    size_t tail;
    RT_MUTEX lock;
};

static struct read_buffer rb;

#define SIGNAL_GEN_PERIOD       ((RTIME)(1000000UL))
static RT_TASK wave_generator_rt_task;

/**
 * The overloading task is running with a period of 10ms, and will generate a CPU consumption of 50% 
 * when active. The activation is checked thanks to the variable overloading.
 * Its priority is 90. It is high, but allows other tasks to take correcting actions if required.
 */
#define OVERLOADING_PERIOD      ((RTIME)(10000000UL))
// #define OVERLOADING_TIME        ((RTIME)(4UL)) // Overload per period, in ms
static bool overloading = false; // Indicates if an overloading shall be generated or not
static RT_TASK overloading_generator_rt_task;

static RTIME overload_value = ((RTIME)4UL);
void set_overload_value(int value)
{
    if (value < 0) {
        printf("Error with the value of set_overload_value.");
        exit(1);
    }
    if (value > 10) {
        printf("Error with the value of set_overload_value.");
        exit(1);
    }
    overload_value = ((RTIME)value);
}

static void check_io()
{
    static uint32_t old_buttons = 0xFFFFFFFF;

    uint32_t buttons = get_buttons();
    uint32_t switches = get_switches();

    /* Note : There surely is a better way. */
    if (switches & SW0) {
        if (is_pressed_now(old_buttons, buttons, KEY0)) {
            current_freq = FREQ0;
        } else if (is_pressed_now(old_buttons, buttons, KEY1)) {
            current_freq = FREQ1;
        } else if (is_pressed_now(old_buttons, buttons, KEY2)) {
            current_freq = FREQ2;
        }
    } else {
        if (is_pressed_now(old_buttons, buttons, KEY0)) {
            current_wave = TRIANGULAR;
        } else if (is_pressed_now(old_buttons, buttons, KEY1)) {
            current_wave = SINUSOID;
        } else if (is_pressed_now(old_buttons, buttons, KEY2)) {
            current_wave = SQUARE;
        }
    }
    old_buttons = buttons;

    overloading = switches & SW1;
}

static void wave_generator_task(void *cookie)
{
    const double sampling_freq = (double) 1024;
    const double sampling_period = 1.0 / sampling_freq;
    const uint64_t s_p = (uint64_t)(sampling_period * 1000000000UL);
    const uint64_t g_p = (uint64_t)(SIGNAL_GEN_PERIOD); // A voir en fonction de l'unité de la période

    // Number of samples to generate per generation period, rounded down
    const uint64_t nb_per_period = g_p / s_p;
    // The ratio indicates the percentage of generation periods where we should
    // generate 1 sample more.
    const double ratio = ((double)(g_p - nb_per_period * s_p)) / ((double)(s_p));

    rt_task_set_periodic(&wave_generator_rt_task, TM_NOW, SIGNAL_GEN_PERIOD);

    while (1) {

        /* Check if there is a change in signal type or frequency */
        check_io();

        // each generation period, generate a random number to choose to generate
        // nb_per_period or nb_per_period + 1.
        // The probability of generating a +1 corresponds to the pre-calculated
        // ratio
        double x = (double)rand()/(double)(RAND_MAX);
        uint64_t nb_to_gen_now;
        if (x < ratio) {
            nb_to_gen_now = nb_per_period + 1;
        }
        else {
            nb_to_gen_now = nb_per_period;
        }

        rt_mutex_acquire_timed(&rb.lock, NULL);

        // Only write data if there is room for it
        uint64_t real_nb_to_gen = nb_to_gen_now;
        if (real_nb_to_gen > rb.capacity - rb.size) {
            real_nb_to_gen = rb.capacity - rb.size;
        }

        size_t new_wave_idx = (wave_idx[current_wave] + real_nb_to_gen * current_freq * 8.0);
        if ((size_t)(new_wave_idx) > SIGNAL_BUF_SIZE) {
            new_wave_idx -= SIGNAL_BUF_SIZE;
        }

        /* Iterate until end of circular buffer */
        for (int sample_i = 0; sample_i < real_nb_to_gen; sample_i ++) {
            rb.buffer[rb.head] = wave[current_wave][(size_t)(wave_idx[current_wave])];
            rb.head = (rb.head + 1) % rb.capacity;
            wave_idx[current_wave] = (wave_idx[current_wave] + current_freq*8.0);
            if ((size_t)(wave_idx[current_wave]) > SIGNAL_BUF_SIZE) {
                wave_idx[current_wave] -= SIGNAL_BUF_SIZE;
            }
            
        }

        wave_idx[current_wave] = new_wave_idx;

        /* Update size */
        rb.size = rb.size + real_nb_to_gen;
        rt_mutex_release(&rb.lock);

        rt_task_wait_period(NULL);
    }
}

static void overloading_generator_task(void *cookie)
{

    int ret = rt_task_set_periodic(&overloading_generator_rt_task, TM_NOW, OVERLOADING_PERIOD);
    if (ret != 0) {
        printf("Error with setting the period of the overloading generator.");
        exit(1);
    }
    while (1) {
        if (overloading) {
            busy_cpu(overload_value);
        }
        rt_task_wait_period(NULL);
    }
}


int init_board(size_t buf_capacity)
{
    /* Ouverture du driver RTDM */
    rtsnd_fd = open("/dev/rtdm/snd", O_RDWR);
    if (rtsnd_fd < 0) {
        perror("Opening /dev/rtdm/snd");
        exit(EXIT_FAILURE);
    }

    ioctrls = mmap(NULL, 4096, PROT_READ|PROT_WRITE, MAP_SHARED, rtsnd_fd, 0);
    if (ioctrls == MAP_FAILED) {
        perror("Mapping real-time sound file descriptor");
        exit(EXIT_FAILURE);
    }

    for (size_t i = 0; i < NB_WAVES; i++) {
        wave[i] = malloc(sizeof(data_t) * SIGNAL_BUF_SIZE);
        if (wave[i] == NULL) {
            perror("Allocating memory for wave buffer");
            exit(EXIT_FAILURE);
        }
    }

    rb.buffer = malloc(sizeof(data_t) * buf_capacity);
    if (rb.buffer == NULL) {
        perror("Allocating memory for read buffer");
        exit(EXIT_FAILURE);
    }

    /* Triangle wave */
    for (size_t i = 0; i < SIGNAL_BUF_SIZE/2 + 1; i++) {
        wave[TRIANGULAR][i] = i;
    }
    for (size_t i = 0; i < SIGNAL_BUF_SIZE/2 - 1; i++) {
        wave[TRIANGULAR][SIGNAL_BUF_SIZE - 1 - i] = i + 1;
    }

    /* Sinus wave */
    for (size_t i = 0; i < SIGNAL_BUF_SIZE; i++) {
        wave[SINUSOID][i] = (double)SIGNAL_BUF_SIZE/2/2 * (1 + sin(((double)i / SIGNAL_BUF_SIZE) * 2 * M_PI));
    }

    /* Square wave */
    for (size_t i = 0; i < SIGNAL_BUF_SIZE/2; i++) {
        wave[SQUARE][i] = 0;
    }
    for (size_t i = SIGNAL_BUF_SIZE/2; i < SIGNAL_BUF_SIZE; i++) {
        wave[SQUARE][i] = SIGNAL_BUF_SIZE/2;
    }

    rb.capacity = buf_capacity;
    rb.head = 0;
    rb.tail = 0;
    rb.size = 0;
    rt_mutex_create(&rb.lock, "read buffer lock");

    rt_task_spawn(&wave_generator_rt_task, "wave generator", 0, 99, T_JOINABLE, wave_generator_task, NULL);

    rt_task_spawn(&overloading_generator_rt_task, "overloading generator", 0, 90, T_JOINABLE, overloading_generator_task, NULL);

    return 0;
}

int clean_board(void)
{
    /* Violently kill the generator task.
     * FIXME : change with a more civilised way of stopping it (global flag
     * or message sending ) */
    rt_task_delete(&wave_generator_rt_task);

    /* Violently kill the overload generator task.
     * FIXME : change with a more civilised way of stopping it (global flag
     * or message sending ) */
    rt_task_delete(&overloading_generator_rt_task);

    if (munmap(ioctrls, 4096) == -1) {
        perror("Unmapping");
        return -1;
    }

    close(rtsnd_fd);
    rt_mutex_delete(&rb.lock);
    for (size_t i = 0; i < NB_WAVES; i++) {
        free(wave[i]);
    }
    free(rb.buffer);

    return 0;
}

size_t available_samples()
{
    rt_mutex_acquire_timed(&rb.lock, NULL);
    const size_t size = rb.size;
    rt_mutex_release(&rb.lock);
    return size;
}

size_t read_samples(data_t *buf, size_t size)
{
    size_t max_samples = size;

    rt_mutex_acquire_timed(&rb.lock, NULL);
    if (max_samples > rb.size) {
        max_samples = rb.size;
    }

    for (size_t i = 0; i < max_samples; i++) {
        buf[i] = rb.buffer[rb.tail];
        rb.tail = (rb.tail + 1) % rb.capacity;
    }
    rb.size -= max_samples;
    rt_mutex_release(&rb.lock);

    return max_samples;
}

uint32_t get_buttons(void)
{
    return ~keys(ioctrls);
}

uint32_t get_switches(void)
{
    return switches(ioctrls);
}

int is_pressed_now(uint32_t buttons_before, uint32_t buttons_now, uint8_t bit)
{
    uint32_t before_bit = buttons_before & bit;
    uint32_t now_bit = buttons_now & bit;

    if (!now_bit && before_bit) return 1;
    else return 0;
}

void set_led(uint8_t led_num, uint8_t state)
{
    uint32_t current = get_leds(ioctrls);
    uint32_t after = (current & ~(1 << led_num)) | (state << led_num);

    set_leds(ioctrls, after);
}

void display_value(int value)
{
    set_display_value(ioctrls, value);
}

