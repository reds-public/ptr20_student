#ifndef IO_UTILS_H
#define IO_UTILS_H

#include <stdint.h>

#define MS 1000000

/* Retourne l'état des boutons poussoirs (attention, ils sont actifs bas) */
uint32_t keys(void *ioctrl);

/* Retourne l'état des switchs */
uint32_t switches(void *ioctrl);

uint32_t get_leds(void *ioctrl);

void set_leds(void *ioctrl, uint32_t leds);

void set_display_value(void *ioctrl, int value);

#endif // IO_UTILS_H
