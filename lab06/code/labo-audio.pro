TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        board.c \
        io_utils.c \
        acquisition.c

HEADERS += \
    board.h \
    io_utils.h

INCLUDEPATH += libxenomai/usr/xenomai/include


DESTDIR=$$PWD/libxenomai
XENOCONFIG=$$DESTDIR/usr/xenomai/bin/xeno-config

QMAKE_CC=	$(shell DESTDIR=$$DESTDIR $$XENOCONFIG --cc)
QMAKE_LINK = $(shell DESTDIR=$$DESTDIR $$XENOCONFIG --cc)
QMAKE_CFLAGS= -std=c99 $(shell DESTDIR=$$DESTDIR $$XENOCONFIG --posix --alchemy --cflags)
QMAKE_LFLAGS=$(shell DESTDIR=$$DESTDIR $$XENOCONFIG --posix --alchemy --ldflags --auto-init-solib)
