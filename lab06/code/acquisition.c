#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <math.h>

#include <cobalt/stdio.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/event.h>
#include <alchemy/queue.h>

#include "board.h"

/* NOTE : Si nécessaire, vous pouvez changer la période des tâches */
#define ACQUISITION_TASK_PERIOD         ((RTIME)1000000UL)
static RT_TASK acquisition_rt_task;
#define PROCESSING_TASK_PERIOD          ((RTIME)1000000UL)
static RT_TASK processing_rt_task;
#define LOG_TASK_PERIOD                 ((RTIME)1000000UL)
static RT_TASK log_rt_task;

#define DFT_BINS                        1024    // Number of samples for the DFT -> can only analyze up to 512 Hz

/** 
 * @brief Applies a DFT on a sequence of samples in the time domain. It outputs a power 
 * distribution over the frequency domain.
 * 
 * @param power A preallocated buffer for storing the result of the DFT. Must be of size at
 * least *sample_count* in number of elements.
 * @param x A buffer containing the sequence of samples. Must be of size at least
 * *sample_count*.
 * @param sample_count The number of samples to consider.
 * @param im A preallocated buffer necessary for the DFT of size at least *sample_count*. 
 * Since we are in a Real-time environnement we don't want to allocate on the fly directly
 * in the function
 * @param re Same as *im*
 */
void dft(float *power, const data_t *x, const size_t sample_count, float *im, float *re)
{
    for (size_t k = 0; k < sample_count; k++) {
        re[k] = 0;
        for (size_t n = 0; n < sample_count; n++) {
            re[k] += x[n] * cos(n * k * 2 * M_PI / sample_count);
        }

        im[k] = 0;
        for (size_t n = 0; n < sample_count; n++) {
            im[k] -= x[n] * sin(n * k * 2 * M_PI / sample_count);
        }

        power[k] = re[k] * re[k] + im[k] * im[k];
    }
}

void log_task(void *cookie)
{
    /* TODO : afficher à la console la fréquence ayant la puissance maximale 
     * et le temps nécessaire pour calculer cette valeur. 
     * Exemple d'affichage : Dominant frequency : 64Hz, computation time : 800ms */
}

void acquisition_task(void *cookie)
{
    /* TODO : reprendre et adapter le code d'exemple ci-dessous pour
     * effectuer une acquisition d'un certain nombre d'échantillons puis les
     * envoyer à la tâche *processing_task*.
     * 
     * Vous allez effectuer un échantillonage d'un nombre d'éléments égal à
     * DFT_BINS. C'est sur cette ensemble entier que la tâche processing doit
     * appliquer son traitement. 
     * 
     * Notez bien que le générateur effectue un échantillonage fixé à 1024 Hz.
     */
    rt_task_set_periodic(&acquisition_rt_task, TM_NOW, ACQUISITION_TASK_PERIOD);

    data_t *samples, *samples_p;
    samples = malloc(sizeof(data_t) * DFT_BINS);

    while (1) {
        size_t to_read = DFT_BINS;
        samples_p = samples;
        while (to_read != 0) {
            rt_task_wait_period(NULL);
            while (available_samples() > 0 && to_read != 0) {
                size_t read = read_samples(samples_p, to_read);
                samples_p += read;
                to_read -= read;
            }
        }
    }
}

void processing_task(void *cookie)
{
    /* TODO : compléter la tâche processing de telle sorte qu'elle recoive les 
     * échantillons de la tâche acquisition. Une fois reçu les échantillons,
     * appliquer une DFT à l'aide de la fonction dft fournie, puis trouver
     * la fréquence à laquelle la puissance est maximale.
     * Enfin, envoyer à la tâche *log* le résultat, ainsi
     * que le temps qui a été nécessaire pour effectuer le calcul. */
}

int
main(int argc, char *argv[])
{
    size_t sampling_buffer_size;

    if (argc < 2) {
        printf("Not enough arguments. \
		Expected %s <bufferSize> \n", argv[0]);
        return EXIT_SUCCESS;
    }

    sampling_buffer_size = atol(argv[1]);

    if (init_board(sampling_buffer_size) != 0) {
        perror("Error at board initialization.");
        exit(EXIT_FAILURE);
    }
    printf("Init board successfull\n");

    mlockall(MCL_CURRENT | MCL_FUTURE);

    if (rt_task_spawn(&acquisition_rt_task, "acquisition task", 0, 99, T_JOINABLE, acquisition_task, NULL) != 0) {
        perror("Error while starting acquisition_task");
        exit(EXIT_FAILURE);
    };
    printf("Launched acquisition task\n");
    if (rt_task_spawn(&processing_rt_task, "processing task", 0, 99, T_JOINABLE, processing_task, NULL) != 0) {
        perror("Error while starting processing_task");
        exit(EXIT_FAILURE);
    };
    printf("Launched processing task\n");
    if (rt_task_spawn(&log_rt_task, "log task", 0, 99, T_JOINABLE, log_task, NULL) != 0) {
        perror("Error while starting log task");
        exit(EXIT_FAILURE);
    };
    printf("Launched log task\n");

    rt_task_join(&acquisition_rt_task);
    rt_task_join(&processing_rt_task);
    rt_task_join(&log_rt_task);

    clean_board();
    munlockall();

    return EXIT_SUCCESS;
}
