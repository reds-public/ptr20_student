#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <cobalt/stdio.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/event.h>

#include "board.h"

struct io_task_args {
    /* TODO */
};

struct write_task_args {
    data_t *samples_buf;
    size_t samples_buf_size;

    uint64_t freq;
    /* TODO: modify if necessary */
};

static RT_TASK io_rt_task;
static RT_TASK write_rt_task;

void io_task(void *cookie)
{

    set_led(LED0, LED_LOW);
    /* TODO */
}

void write_task(void *cookie)
{
    struct write_task_args *args = (struct write_task_args*)cookie;

    char output_filename[256];
    data_t *samples_buf = args->samples_buf;
    const size_t samples_buf_size = args->samples_buf_size;

    FILE *output_file;
    int file_index = 0;

    /* Open a file to write into */
    sprintf(output_filename, "test_%d.txt", file_index);
    output_file = fopen(output_filename, "w+");
    if (output_file == NULL) {
        perror("Could not open the output file");
        exit(EXIT_FAILURE);
    }
    rt_printf("Start recording in file %s\n", output_filename);

    /* Read samples continuously */
    /* TODO : make it periodic and add IO control to start/stop
     * a new data acquisition in a different file */
    while (1) {
        size_t actually_read;
        size_t to_read = available_samples();
        to_read = to_read > samples_buf_size ? samples_buf_size : to_read;

        actually_read = read_samples(samples_buf, to_read);
        for (size_t i = 0; i < actually_read; i++) {
            while (fprintf(output_file, "%u\n", samples_buf[i]) <= 0);
        }
    }
}

int main(int argc, char *argv[])
{
    struct io_task_args io_args;
    struct write_task_args write_args;

    size_t sampling_buffer_size;
    uint64_t freq;

    if (argc < 4) {
        printf("Not enough arguments. \
                Expected %s <freqSample> <bufferSize> <freqTask>.\n", argv[0]);
        return EXIT_SUCCESS;
    }

    freq = atol(argv[1]);
    sampling_buffer_size = atol(argv[2]);
    write_args.freq = atol(argv[3]);

    if (init_board(sampling_buffer_size, freq) != 0) {
        perror("Error at board initialization.");
        exit(EXIT_FAILURE);
    }

    write_args.samples_buf = malloc(sizeof(data_t) * 1024);
    write_args.samples_buf_size = 1024;

    mlockall(MCL_CURRENT | MCL_FUTURE);

    if (rt_task_spawn(&io_rt_task, "io task", 0, 99, T_JOINABLE, io_task, &io_args) != 0) {
        perror("Error while starting io_task");
        exit(EXIT_FAILURE);
    };
    if (rt_task_spawn(&write_rt_task, "write task", 0, 50, T_JOINABLE, write_task, &write_args) != 0) {
        perror("Error while starting write_task");
        exit(EXIT_FAILURE);
    };
    rt_task_join(&io_rt_task);
    rt_task_join(&write_rt_task);

    clean_board();
    munlockall();

    return EXIT_SUCCESS;
}
