////////////////////////////////////////////////////////////////////////////////
// File : check_acquisition.c
//
// Author : Yann Thoma
//
// Date : 15.11.2020
//
// Description: This program analysis a text file containing integers in
//              text format. It checks that two consecutive numbers do not
//              differ by more than 1.
////////////////////////////////////////////////////////////////////////////////



#include <stdio.h>                 // Needed for printf() and feof()
#include <stdlib.h>                // Needed for exit() and atof()
#include <string.h>                // Needed for strcmp()

////////////////////////////////////////////////////////////////////////////////
//   analyze_file
//
// Analysis of the file, looking for errors in the value.
// It is expected to get adjacent values (positive or negative difference).
////////////////////////////////////////////////////////////////////////////////

int analyze_file(char *filename)
{
    char      temp_string[1024];     // Temporary string variable
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
        printf("Can not open file %s\n", filename);
        return 1;
    }

    int last_value;
    int first = 1;
    int line = 0;

    // Read all values
    while(1)
    {
        fscanf(f, "%s", temp_string);
        if (feof(f)) goto end;
        int value = atoi(temp_string);
        if (first) {
            first = 0;
        }
        else {
            if (abs(last_value - value) != 1) {
                printf("Error at line %d\n", line);
            }
        }
        line ++;
        last_value = value;
        // printf("Value : %d\n", value);
    }

    end:

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//   Main program
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Missing program argument (filename).");
    }
    char *filename = argv[1];

    return analyze_file(filename);
}
